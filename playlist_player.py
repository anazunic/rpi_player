
from omx_player import OMXPlayer
from threading  import Thread
import time

class PlaylistPlayer(Thread):

    def __init__(self, items):
        Thread.__init__(self)
        self.playlist_items = items
        self._iterator = 0
        self._loop_flag = False
        self._omx = OMXPlayer()

    def run(self):
        
        self._loop_flag =True
        while(self._loop_flag):

            curr_vid_path = self.playlist_items[self._iterator]["path"]
            curr_vid_duration = self.playlist_items[self._iterator]["duration"]
            
            self._omx.start(curr_vid_path)

            for i in range(0, curr_vid_duration):
                if(self._loop_flag == False):
                    break
                else:
                    time.sleep(1)

            self._iterator += 1
            if(self._iterator >= len(self.playlist_items)):
                self._iterator = 0

    def stop(self):
        self._loop_flag = False
        self._omx.stop()
        
