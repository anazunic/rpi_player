import pexpect

class OMXPlayer(object):

    """ Klasa za upravljanje OMX video playerom """

    """ OMX player """
    #_LAUNCH_CMD = "/usr/bin/omxplayer --no-osd {} > /dev/null"

    _LAUNCH_CMD = '/usr/bin/omxplayer --win "0 0 500 500" --no-osd {} > /dev/null'
    _QUIT_CMD = 'q'
    
    def __init__(self):
        self._process = None

    def start(self, path):
        cmd = self._LAUNCH_CMD.format(path)
        self._process = pexpect.spawn(cmd)

    def stop(self):
        """Stops OMXPlayer process."""
        if self._process:
            self._process.send(self._QUIT_CMD)
            self._process.terminate(force=True)
            

