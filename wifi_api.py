
import time

import subprocess
import os

from json_response import JSONResponse
from logger_api import LoggerApi


class WifiApi(JSONResponse):

    def __init__(self):
        self._mac = subprocess.getoutput("ifconfig eth0 | awk '$0 - /ether /{ print $2 }'")
        self._logger = LoggerApi()
        self._url = self._DOMAIN + ("api/wifi/{}".format(self._mac))

    def get_wifi(self):
        
        """Get WiFi setting form server and set up it on device."""
        data = self.get_json(self._url)

        print("Wifi data: ")
        print(data)
        
        if "status" in data:
            if data["status"] == 0:

                masked_password = "*" * len(data["networkPassword"])
                
                self._logger.log("Nove wifi postavke primljene! Ime mreže: {}, lozinka: {}, tip: {}".format(
                    data["networkName"],
                    masked_password,
                    data["networkType"]
                    ))

                self.update_wpa_supplicant(data["networkName"], data["networkPassword"], data["networkType"])
                
                os.system("sudo ifconfig eth0 0.0.0.0")
                os.system("sudo ifconfig wlan0 0.0.0.0")
                os.system("sudo ifconfig wlan1 0.0.0.0")
                os.system("sudo systemctl daemon-reload")
                os.system("sudo systemctl restart dhcpcd")

                time.sleep(30)

                ip = os.popen("sudo ifconfig wlan0 | grep 'inet ' | awk -F: '{print $1}' | awk '{print $2}'").read()
                        
                network = os.popen("sudo grep 'ssid' /etc/wpa_supplicant/wpa_supplicant.conf | cut -d'=' -f 2").read()
                print("name is: {}".format(network))

                password = os.popen("sudo grep 'psk' /etc/wpa_supplicant/wpa_supplicant.conf | cut -d'=' -f 2").read()
                psw = "*" * len(password)
                print("password is: {}".format(psw))

                if(ip != ""):
                    self._logger.log("Uspjesno spajanje na wifi mrezu, IP adresa: {}, Naziv: {}, Lozinka: {}".format(ip, network, psw))    
                else:
                    self._logger.log("Neuspjelo spajanje na wifi mrezu, Naziv: {}, Lozinka: {}".format(data["networkName"], masked_password))


    def update_wpa_supplicant(self, name, password, net_type):
        
        '''
            ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
            update_config=1
            country=HR

            network={
                    ssid="naziv mreze"
                    psk="lozinka"
                    key_mgmt=WPA-PSK
            } 
        '''
        
        wes = open('/home/pi/wireless_encryption_settings.conf', 'w+')
        wes.write("ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev\n")
        wes.write("update_config=1\n\n")
        wes.write("country=HR\n")
        wes.write("network={\n")

        #network name
        wes.write('    ssid="{}"\n'.format(name))
        wes.write('    scan_ssid=1\n') #da se spaja i na skrivene wifi mreze

        #network password
        if net_type == "WEP":
            wes.write('    wep_key0="{}"\n'.format(password))
        elif net_type == "WPA/WPA2":
            wes.write('    psk="{}"\n'.format(password))
        elif net_type == "OPEN":
            wes.write('    key_mgmt=NONE')

        wes.write("}\n")
        wes.close()
        os.system("sudo cp /home/pi/wireless_encryption_settings.conf /etc/wpa_supplicant/wpa_supplicant.conf")            
        
#wifi = WifiApi()
#wifi.get_wifi()

