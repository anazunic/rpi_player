
import time
import urllib
import subprocess

from json_response      import JSONResponse


class LoggerApi(JSONResponse):

    def __init__(self):
        self._mac = subprocess.getoutput("ifconfig eth0 | awk '$0 - /ether /{ print $2 }'")
        self._error = None
        self._error_start_time = None
        self._url = self._DOMAIN + "api/newLog/{}".format(self._mac)

    def log(self, message):
        
        #print(real_time)
        if self._error is not None:
            print("error recognized...")
            resp = self.get_json(self._url + "/{}".format(self._error))
            if "status" in resp and resp["status"] == 0:
                self._error = None

        #message = urllib.parse.quote(message.encode('utf8'))
        message = urllib.parse.quote(message, safe='')
        full_url = self._url + '/{}'.format(message)
        print(full_url)
        data = self.get_json(full_url)
        print("data")
        print(data)

        if "status" in data:
            
            if data["status"] == 1:
                real_time = time.strftime('%H:%M:%S')
                self._error_start_time = real_time
                print("Internet connection lost at: {}".format(self._error_start_time))
                msg = "Komunikacija sa serverom izgubljena u {}!".format(self._error_start_time)
                self._error = msg


