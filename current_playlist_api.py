#!/usr/bin/env python3

from json_response import JSONResponse
from omx_player import OMXPlayer
from logger_api import LoggerApi
import subprocess
import time
import os

class CurrentPlaylistApi(JSONResponse):
    
    __API_URL = "api/currentPlaylist/{}"

    def __init__(self):
        self._mac = subprocess.getoutput("ifconfig eth0 | awk '$0 - /ether /{ print $2 }'")
        self._data = { "status": 1, "message": "error" }
        self._playing = False
        self._updated_time = None
        self._start_time = None
        self._pl_id = None
        self._total_vids = None
        self._pl_vids = None
        self._destination = "/home/pi/digitalSignage/current/"
        self._info_file = "/home/pi/digitalSignage/current/info.txt"
        self._play_file = "/home/pi/digitalSignage/current/play.txt"

        self._temp_destination = "/home/pi/digitalSignage/current/temp/"
        self._temp_info_file = "/home/pi/digitalSignage/current/temp/info.txt"
        self._temp_play_file = "/home/pi/digitalSignage/current/temp/play.txt"
        self._loaded_videos = []
        self._omx = OMXPlayer()
        self._logger = LoggerApi()

    def write_infos(self):
        text_file = open(self._temp_info_file, "w+")
        line = str(self._pl_id) + "#" + str(self._updated_time)
        text_file.write(line)
        text_file.close()
        os.system("chmod 777 {}".format(self._temp_info_file))

    def read_infos(self):
        with open(self._info_file) as f:
            for line in f:
                splitted = line.split('#')
                self._pl_id = splitted[0]
                self._updated_time = splitted[1]

    def write_play(self):
        text_file = open(self._temp_play_file, "w+")
        for vid in self._pl_vids:    
            line = str(self._destination + vid["name"]) + "\t" + str(vid["duration"]) + "\n"
            text_file.write(line)
        text_file.close()
        os.system("chmod 777 {}".format(self._temp_play_file))

    def get_data(self):
        self._logger.log("Dohvaćam podatke sa servera")
        full_api_url = self._DOMAIN + self.__API_URL
        full_api_url = full_api_url.format(self._mac)
        
        data = self.get_json(full_api_url)
        print(data)
        
        if data["status"] == 0:
            print(data)
            self._updated_time = data["updatedAt"]
            self._start_time = data["startTime"]
            self._pl_id = data["playlistID"]
            self._total_vids = data["totalVideos"]
            self._pl_vids = data["videos"]
            self._logger.log("Podaci o popisu uspješno dohvaćeni")
            
        else:
            self._logger.log("Dogodila se greška kod dohvaćanja podataka")
            print("error on getting data")

    def check_new_playlist(self):
        print("check_new_playlist")
        full_api_url = self._DOMAIN + self.__API_URL
        full_api_url = full_api_url.format(self._mac)
        
        data = self.get_json(full_api_url)
        print(data)
        if data["status"] == 0:
            if(data["updatedAt"] != self._updated_time or str(data["playlistID"]) != str(self._pl_id)):
                print("on device: ")
                print(self._updated_time)
                print(self._pl_id)
                print("on server: ")
                print(data["updatedAt"])
                print(data["playlistID"])
                
                return True
            else:
                return False
        return False

    def download_video(self, video_dict):
        print("inside download_video")
        print(video_dict)
        comm = ["wget", "-c", "-O", self._temp_destination + video_dict["name"],
                "-t", "15000", "-T", "5", video_dict["videoURL"]]
        print("pozivam comm")
        proc = subprocess.Popen(comm, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        tmp = str(proc.stdout.read())
        print("pocetni tmp")
        print(tmp)
        if "wget: unable to resolve host address" in tmp:
            print("download_video fail")
            return False
        else:
            print("download done")
            return True

    def prepare_playlist(self):
        print("prepare_playlist")
        self._logger.log("Preuzimam video datoteke iz popisa")
        for i in range(0, self._total_vids):
            self._logger.log("Preuzimam video {} - {}/{}".format(self._pl_vids[i]["name"], i+1, self._total_vids))
            self.download_video(self._pl_vids[i])
            self._logger.log("Video {} uspješno sačuvan".format(self._pl_vids[i]["name"]))

    def read_play_file(self):
        total = 0
        self._logger.log("Učitavam videa")
        self._loaded_videos = []
        with open(self._play_file) as f:
            for line in f:
                splitted = line.split('\t')
                vid = { "path": splitted[0], "duration": int(splitted[1]) }
                self._loaded_videos.append(vid)
                total += 1
        self._total_vids = total

    def move_files(self):
        os.system("sudo chmod 777 -R /home/pi/digitalSignage")
        os.system("mv /home/pi/digitalSignage/current/temp/* /home/pi/digitalSignage/current/")
        
    def delete_files(self):
        self._logger.log("Uklanjam stari popis")
        for f in os.listdir(self._destination):
            full_path = os.path.join(self._destination, f)
            if os.path.isfile(full_path):
                os.unlink(full_path)

    def load_videos(self):
        self._logger.log("Priremam popis")
        if os.path.isfile(self._play_file) and os.stat(self._play_file).st_size != 0:
            print("play.txt found!")
            self._logger.log("Popis je već spremljen na uređaju")
            self.read_infos()
            self.read_play_file()
            self._logger.log("Videa ucitana")
            return True
        else:
            print("play.txt does not exist, download and prepare playlist")
            self._logger.log("Popis ne postoji, preuzimam popis")
            self.get_data()
            self.write_infos()
            self.write_play()
            self.prepare_playlist()
            self.move_files()
            self.read_play_file()
            self._logger.log("Videa učitana")
            return True
        

#api = CurrentPlaylistApi()
#api.load_videos()

