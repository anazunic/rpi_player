from check_device_api           import CheckDeviceApi
from current_playlist_api       import CurrentPlaylistApi
from playlist_player            import PlaylistPlayer
from wifi_api                   import WifiApi
from logger_api                 import LoggerApi
import threading
from threading import Thread

import subprocess
import time
import os

class Start:

    def __init__(self):
        self._mac = subprocess.getoutput("ifconfig eth0 | awk '$0 - /ether /{ print $2 }'")
        self.check_device_api = CheckDeviceApi()
        self.cpa = CurrentPlaylistApi()
        self.wifi = WifiApi()
        self.logger = LoggerApi()
        self.pp = None
        
    def start_player(self):
        self.logger.log("Uređaj pokrenut!")
        device_exist = self.check_device_api.get_data()
        if(device_exist):
            print("uređaj postoji u sustavu")
            print(device_exist)
            self.main_loop()
        else:
            print("device not in system!")
            os.system("sudo fbi -T 2 -a /home/pi/digitalSignage/uredjaj.png")

    def send_alive_log(self):
        self.logger.log("Uređaj u funkciji!")

    def main_loop(self):

        self.cpa.load_videos()
        print(self.cpa._loaded_videos)
        self.pp = PlaylistPlayer(self.cpa._loaded_videos)
        self.logger.log("Reprodukcija popisa započinje!")
        self.pp.start()
        time.sleep(5)
        alive_log_counter = 0
        
        while True:

            if(self.cpa.check_new_playlist() == True):
                self.logger.log("Novi popis prepoznat!")
                self.cpa.get_data()
                self.cpa.write_infos()
                self.cpa.write_play()
                self.cpa.prepare_playlist()
                self.pp.stop()
                self.cpa.delete_files()
                self.cpa.move_files()
                self.cpa.read_play_file()
                self.pp.stop()
                print("stop done")
                self.pp.join()
                print("join done")
                print(self.cpa._loaded_videos)
                self.pp = PlaylistPlayer(self.cpa._loaded_videos)
                self.logger.log("Reprodukcija popisa započinje!")
                self.pp.start()

            self.wifi.get_wifi()
            time.sleep(30)
            alive_log_counter += 30
            
            if(alive_log_counter >= 300):
                print("šaljem alive log!")
                self.send_alive_log()
                alive_log_counter = 0
                    

s = Start()
s.start_player()
