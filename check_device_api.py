from json_response import JSONResponse
import subprocess

class CheckDeviceApi(JSONResponse):
    
    __API_URL = "api/deviceInDatabase/{}"

    def __init__(self):
        self._mac = subprocess.getoutput("ifconfig eth0 | awk '$0 - /ether /{ print $2 }'")

    def get_data(self):
        full_api_url = self._DOMAIN + self.__API_URL
        full_api_url = full_api_url.format(self._mac)
        
        data = self.get_json(full_api_url)
        print("provjera je li uređaj u sustavu: ")
        print(data)
        
        if data["status"] == 0:
            if data["deviceFound"] == True:
                return True
            else:
                return False
        else:
            return False


#api = CheckDeviceApi()
#api.get_data()
