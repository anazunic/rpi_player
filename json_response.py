import requests

class JSONResponse(object):
    
    """ Klasa preko koje se komunicira sa serverom """

    #_DOMAIN = "http://azTechSolutions.com/"
    _DOMAIN = "http://192.168.1.250:8080/"
    
    def get_json(self, url):
        
        data = {"status": 1, "message": "error"}
        try:
            request = requests.get(url, timeout=2)
            data = request.json()
        except:
            data["status"] = 1
            data["message"] = "Greška u komunikaciji sa serverom"
        
        return data
